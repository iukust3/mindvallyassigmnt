package com.example.mindvallaypinboardassigment.Fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cellyco.mightyutils.BiConsumer;
import com.example.mindvallaypinboardassigment.Adapter.StoryAdapter;
import com.example.mindvallaypinboardassigment.Models.PinterestModel;
import com.example.mindvallaypinboardassigment.R;
import com.example.mindvallaypinboardassigment.Utials.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.irfan.mindvalleyUtials.json.GsonUtil.GSONUTIL;


public class ListFragment extends Fragment {

    public ListFragment() {
        // Required empty public constructor
    }


    public static ListFragment newInstance() {
        ListFragment fragment = new ListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        RecyclerView recyclerView=view.findViewById(R.id.recyclerview);
        final StoryAdapter storyAdapter=new StoryAdapter(getContext(),new ArrayList<PinterestModel>());
        recyclerView.setAdapter(storyAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        GSONUTIL.getJsonFromServer(Constant.URL, (var1, var2) -> {
            if(var2==null) {
                Gson gson = new Gson();
                Type listType = new TypeToken<List<PinterestModel>>() {
                }.getType();
                List<PinterestModel> pinterestModels = gson.fromJson(var1, listType);
                storyAdapter.setList(pinterestModels);
            }else {
                var2.printStackTrace();
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }
}
