package com.example.mindvallaypinboardassigment.Fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cellyco.mightyutils.BiConsumer;
import com.example.mindvallaypinboardassigment.Adapter.StoryAdapter;
import com.example.mindvallaypinboardassigment.Models.PinterestModel;
import com.example.mindvallaypinboardassigment.R;
import com.example.mindvallaypinboardassigment.Utials.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.irfan.mindvalleyUtials.json.GsonUtil;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.irfan.mindvalleyUtials.json.GsonUtil.GSONUTIL;

public class DetailFragment extends Fragment {

    private static final String ARG_PARAM1 = "Detail";

    // TODO: Rename and change types of parameters
    private PinterestModel pinterestModel;


    public DetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param pinterestModel Parameter 1.
     * @return A new instance of fragment DetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailFragment newInstance(PinterestModel pinterestModel) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, pinterestModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pinterestModel = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detial, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
    }
}
