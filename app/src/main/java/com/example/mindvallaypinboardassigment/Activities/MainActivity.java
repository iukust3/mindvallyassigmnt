package com.example.mindvallaypinboardassigment.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Bundle;

import com.example.mindvallaypinboardassigment.R;
import com.irfan.mindvalleyUtials.json.GsonUtil;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GsonUtil.iniate(this);
        NavController navController= Navigation.findNavController(this,R.id.nav_host_fragment);
    }
}
