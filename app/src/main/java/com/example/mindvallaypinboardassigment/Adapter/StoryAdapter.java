package com.example.mindvallaypinboardassigment.Adapter;

import android.content.Context;
import android.os.storage.StorageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.cellyco.mightystorage.service.StorageService;
import com.example.mindvallaypinboardassigment.Models.PinterestModel;
import com.example.mindvallaypinboardassigment.R;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.ViewHolder> {
    private List<PinterestModel> pinterestModelList = new ArrayList<>();
    private Context context;
    private StorageService storageService;

    public StoryAdapter(Context context, List<PinterestModel> models) {
        this.context = context;
        this.pinterestModelList = models;
        this.storageService=new StorageService(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_photo_pin, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PinterestModel model = pinterestModelList.get(position);
        holder.likes.setText(String.format("%d Likes ", model.getLikes()));
        holder.progressBar.setVisibility(View.VISIBLE);
        storageService.downloadImage(model.getUrlDetails().getFull(),holder.thoumbNail,holder.progressBar);
      //  storageService.downloadImage(model.getUser().getProfileImage().getSmall(),holder.profileImage,null);
        holder.userName.setText(model.getUser().getName());
    }

    @Override
    public int getItemCount() {
        return pinterestModelList.size();
    }

    public void setList(List<PinterestModel> pinterestModels) {
        this.pinterestModelList=pinterestModels;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircularImageView profileImage;
        private ImageView thoumbNail;
        private TextView userName;
        private TextView likes;
        private ProgressBar progressBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            profileImage = itemView.findViewById(R.id.user_profile_image);
            thoumbNail = itemView.findViewById(R.id.Thumbnail);
            userName = itemView.findViewById(R.id.userName);
            likes = itemView.findViewById(R.id.userLikes);
            progressBar=itemView.findViewById(R.id.loading);
        }
    }
}
