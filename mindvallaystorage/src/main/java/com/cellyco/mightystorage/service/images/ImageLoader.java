package com.cellyco.mightystorage.service.images;

import android.content.Context;
import android.util.LruCache;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.irfan.mindvalleyUtials.animation.SimpleAnimation;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ImageLoader {
    private final int maxCacheSize;
    private Context context;
    private HashMap<String, Future<ImageView>> imageloadingRequest = new HashMap<String, Future<ImageView>>();
    private LruCache<String, Future<ImageView>> dowanloadCashe;
    private Future<ImageView> imageViewFuture;

    public ImageLoader(Context context) {
        this.context = context;
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        // Use 1/8th of the available memory for this memory cache.
        maxCacheSize = maxMemory / 8; // 4 * 1024 * 1024; // 4MiB
        dowanloadCashe = new LruCache<>(maxCacheSize);

    }

    public void loadNoCache(String url, ImageView imageView) {

        System.out.println("url : " + url);

        Ion.with(imageView.getContext())
                .load(url)
                .noCache()
                .withBitmap()
                .centerCrop()
//                TODO add placeholders
//                .placeholder(R.drawable.common_google_signin_btn_icon_dark)
//                .error(R.drawable.common_google_signin_btn_icon_dark)
                .animateLoad(SimpleAnimation.spin())
                .animateIn(SimpleAnimation.fadeIn())
                .intoImageView(imageView)
        ;

    }

    public void load(String url, ImageView imageView, ProgressBar progressBar) {
        System.err.println("url : " + url);
        if (imageloadingRequest.containsKey(url)) {
            if (dowanloadCashe.get(url) != null) {
                try {
                    imageView.setImageDrawable(dowanloadCashe.get(url).tryGet().getDrawable());
                } catch (Exception e) {
                    e.printStackTrace();
                    imageloadingRequest.remove(url);
                    dowanloadCashe.remove(url);
                    imageViewFuture = Ion.with(imageView).animateLoad(SimpleAnimation.spin())
                            .animateIn(SimpleAnimation.fadeIn()).fitXY().load(url).setCallback(new FutureCallback<ImageView>() {
                                @Override
                                public void onCompleted(Exception e, ImageView result) {
                                    dowanloadCashe.put(url, imageloadingRequest.get(url));
                                   if(progressBar!=null)
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                    imageloadingRequest.put(url, imageViewFuture);
                }
            }
        } else {
            imageViewFuture = Ion.with(imageView).animateLoad(SimpleAnimation.spin())
                    .animateIn(SimpleAnimation.fadeIn()).fitXY().load(url).setCallback(new FutureCallback<ImageView>() {
                        @Override
                        public void onCompleted(Exception e, ImageView result) {
                            try {
                                dowanloadCashe.put(url, imageloadingRequest.get(url));
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    });
            imageloadingRequest.put(url, imageViewFuture);
        }

    }

    public void fromFile(File file, ImageView imageView) {
        String url = "file://" + file.getAbsolutePath();
        Ion.with(imageView)
                .centerCrop()
//                TODO add placeholder
//                .placeholder(R.drawable.common_google_signin_btn_icon_dark)
                .animateLoad(SimpleAnimation.spin())
                .animateIn(SimpleAnimation.fadeIn())
                .load(url);
    }

    public void cancelRequest(String urlAsKey) {
        Objects.requireNonNull(imageloadingRequest.get(urlAsKey)).cancel(true);

    }

    public void cancelAllRequest() {
        for (Map.Entry<String,
                Future<ImageView>> entry : imageloadingRequest.entrySet())
            entry.getValue().cancel();
        imageloadingRequest.clear();
    }

    public void clearCahse() {
        dowanloadCashe.evictAll();
        cancelAllRequest();
    }

}
