package com.cellyco.mightystorage.service;

import android.widget.ImageView;
import android.widget.ProgressBar;

import com.cellyco.mightyutils.BiConsumer;
import com.irfan.mindvalleyUtials.json.JsonObject;

import java.io.File;

public interface IStorageService  {

    void json(String fileName, BiConsumer<JsonObject, Throwable> consumer);
    void image(String fileName, ImageView imageView);
    void file(String fileName, BiConsumer<File, Throwable> consumer);

    void readJson(String fileName, BiConsumer<JsonObject, Throwable> consumer);
    void readImage(String fileName, ImageView imageView);
    void readFile(String fileName, BiConsumer<File, Throwable> consumer);

    void downloadJson(String fileName, BiConsumer<JsonObject, Throwable> consumer);
    void downloadImageToStorage(String fileName, ImageView imageView);
    void downloadImage(String url, ImageView imageView, ProgressBar progressBar);
    void downloadFile(String fileName, BiConsumer<File, Throwable> consumer);
    void cancelRequest(String urlAsKey);
    void cancelAll();
}
