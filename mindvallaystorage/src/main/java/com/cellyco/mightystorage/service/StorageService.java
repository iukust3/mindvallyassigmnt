package com.cellyco.mightystorage.service;

import android.content.Context;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.cellyco.mightystorage.service.images.ImageLoader;
import com.cellyco.mightyutils.BiConsumer;
import com.cellyco.mightyutils.MindValleyThreads;
import com.irfan.mindvalleyUtials.files.FileUtil;
import com.irfan.mindvalleyUtials.json.JsonObject;
import com.koushikdutta.ion.Ion;

import java.io.File;

public class StorageService implements IStorageService {

    private final Context context;
    private final ImageLoader imageLoader;

    public StorageService(Context context) {

        this.context = context;

        imageLoader = new ImageLoader(context);
    }

    /**
     * File from extension storage
     *
     * @param fileName
     * @return
     */
    private File fileFromStorage(String fileName) {
        return FileUtil.newFileInStorage(fileName, context);
    }

    /**
     * If file not exist download and return.
     *
     * @param fileName
     * @param consumer
     */
    @Override
    public void json(String fileName, BiConsumer<JsonObject, Throwable> consumer) {
        if (FileUtil.exist(fileFromStorage(fileName))) {
            readJson(fileName, consumer);
        } else {
            downloadJson(fileName, consumer);
        }
    }

    @Override
    public void image(String fileName, ImageView imageView) {
        if (FileUtil.exist(fileFromStorage(fileName))) {
            imageLoader.fromFile(fileFromStorage(fileName), imageView);
        } else {
            downloadImage(fileName, imageView,null);
        }
    }

    @Override
    public void file(String fileName, BiConsumer<File, Throwable> consumer) {
        if (FileUtil.exist(fileFromStorage(fileName))) {
            readFile(fileName, consumer);
        } else {
            downloadFile(fileName, consumer);
        }

    }

    /**
     * Read file from extension storage
     *
     * @param fileName
     * @param consumer
     */
    @Override
    public void readJson(String fileName, BiConsumer<JsonObject, Throwable> consumer) {
        FileUtil.readJson(fileName, context, consumer);
    }

    /**
     * Read image from extension storage
     *
     * @param fileName
     * @param imageView
     */
    @Override
    public void readImage(String fileName, ImageView imageView) {
        File file = fileFromStorage(fileName);
        imageLoader.fromFile(file, imageView);
    }

    /**
     * Read file from extension storage
     *
     * @param fileName
     * @param consumer
     */
    @Override
    public void readFile(String fileName, BiConsumer<File, Throwable> consumer) {
        consumer.accept(fileFromStorage(fileName), null);
    }

    @Override
    public void downloadJson(String fileName, BiConsumer<JsonObject, Throwable> consumer) {
        downloadFromInet(fileName, getDownloadUrl(fileName),
                (res, th) -> {
                    if (res != null && th == null) {
                        readJson(fileName, consumer);
                    } else {
                        consumer.accept(null, th);
                    }

                });
    }

    @Override
    public void downloadImageToStorage(String fileName, ImageView imageView) {
        downloadFromInet(fileName, getDownloadUrl(fileName), (res, th) -> {
            if (th == null) {
                imageLoader.fromFile(new File(res), imageView);
            }
        });
    }

    @Override
    public void downloadImage(String url, ImageView imageView, ProgressBar progressBar) {
        imageLoader.load(url, imageView,null);
    }

    @Override
    public void downloadFile(String fileName, BiConsumer<File, Throwable> consumer) {
        downloadFromInet(fileName, getDownloadUrl(fileName), (res, th) -> {
            consumer.accept(res != null ? new File(res) : null, th);
        });
    }

    @Override
    public void cancelRequest(String urlAsKey) {
        imageLoader.cancelRequest(urlAsKey);
    }

    @Override
    public void cancelAll() {
        imageLoader.cancelAllRequest();
    }


    public void load(String url, ImageView imageView) {
        if (URLUtil.isValidUrl(url)) {
            imageLoader.load(url, imageView,null);
        } else {
            imageLoader.load(getDownloadUrl(url), imageView,null);
        }
    }

    private String getDownloadUrl(String fileName) {
        return "/download/" + fileName;
    }

    /**
     * This function is design for future support to upload
     * Files as will to server
     *
     * @param imageView image to upload to server
     * @param consumer  which hold reference to response
     */
    public void upload(ImageView imageView, BiConsumer<String, Throwable> consumer) {
        MindValleyThreads.inst()
                .onDisk(() -> FileUtil.saveBitmap(imageView, (s, throwable) -> {
                    if (throwable != null) {
                        consumer.accept(null, throwable);
                    } else {
                        consumer.accept(null, new Throwable("Error uploading image"));
                    }
                }));
    }

    /**
     * This function is used for dowanload any type file
     *
     * @param fileName to be saved in storage
     * @param url      downloadable url
     * @param consumer which hold reference to response
     */

    private void downloadFromInet(String fileName, String url, BiConsumer<String, Throwable> consumer) {
        File file = fileFromStorage(fileName);
        if (URLUtil.isValidUrl(url)) {
            Ion.with(context)
                    .load(url)
                    .write(file)
                    .withResponse()
                    .setCallback((th, response) -> {

                        System.out.println("response : " + response);

                        if (th != null) {
                            file.delete();
                        }

                        consumer.accept(th == null ? file.getAbsolutePath() : null, th);
                    });
        } else {
            consumer.accept(null, new IllegalArgumentException("URL : " + url + " not valid"));
        }
    }
}
