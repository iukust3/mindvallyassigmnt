package com.irfan.mindvalleyUtials.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Json {
    public static ObjectMapper mapper = new ObjectMapper();
    public static ObjectMapper prettyMapper = new ObjectMapper();

    public Json() {
    }

    public static String encode(Object obj) throws EncodeException {
        try {
            return mapper.writeValueAsString(obj);
        } catch (Exception var2) {
            throw new EncodeException("Failed to encode as JSON: " + var2.getMessage());
        }
    }


    public static String encodePrettily(Object obj) throws EncodeException {
        try {
            return prettyMapper.writeValueAsString(obj);
        } catch (Exception var2) {
            throw new EncodeException("Failed to encode as JSON: " + var2.getMessage());
        }
    }

    public static <T> T decodeValue(String str, Class<T> clazz) throws DecodeException {
        try {
            return mapper.readValue(str, clazz);
        } catch (Exception var3) {
            throw new DecodeException("Failed to decode: " + var3.getMessage());
        }
    }

    public static <T> T decodeValue(String str, TypeReference<T> type) throws DecodeException {
        try {
            return mapper.readValue(str, type);
        } catch (Exception var3) {
            throw new DecodeException("Failed to decode: " + var3.getMessage(), var3);
        }
    }

    static Object checkAndCopy(Object val, boolean copy) {
        if (val != null && (!(val instanceof Number) || val instanceof BigDecimal) && !(val instanceof Boolean) && !(val instanceof String) && !(val instanceof Character)) {
            if (val instanceof CharSequence) {
                val = val.toString();
            } else if (val instanceof JsonObject) {
                if (copy) {
                    val = ((JsonObject)val).copy();
                }
            } else if (val instanceof JsonArray) {
                if (copy) {
                    val = ((JsonArray)val).copy();
                }
            } else if (val instanceof Map) {
                if (copy) {
                    val = (new JsonObject((Map)val)).copy();
                } else {
                    val = new JsonObject((Map)val);
                }
            } else if (val instanceof List) {
                if (copy) {
                    val = (new JsonArray((List)val)).copy();
                } else {
                    val = new JsonArray((List)val);
                }
            }
        }

        return val;
    }

    static <T> Stream<T> asStream(Iterator<T> sourceIterator) {
        Iterable<T> iterable = () -> {
            return sourceIterator;
        };
        return StreamSupport.stream(iterable.spliterator(), false);
    }

    static {
        mapper.configure(Feature.ALLOW_COMMENTS, true);
        prettyMapper.configure(Feature.ALLOW_COMMENTS, true);
        prettyMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        SimpleModule module = new SimpleModule();
        module.addSerializer(JsonObject.class, new Json.JsonObjectSerializer());
        module.addSerializer(JsonArray.class, new Json.JsonArraySerializer());
        mapper.registerModule(module);
        prettyMapper.registerModule(module);
    }

    private static class JsonArraySerializer extends JsonSerializer<JsonArray> {
        private JsonArraySerializer() {
        }

        public void serialize(JsonArray value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeObject(value.getList());
        }
    }

    private static class JsonObjectSerializer extends JsonSerializer<JsonObject> {
        private JsonObjectSerializer() {
        }

        public void serialize(JsonObject value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
            jgen.writeObject(value.getMap());
        }
    }
}

