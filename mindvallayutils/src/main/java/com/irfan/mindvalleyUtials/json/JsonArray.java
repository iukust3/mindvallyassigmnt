package com.irfan.mindvalleyUtials.json;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public class JsonArray implements Iterable<Object> {
    private List<Object> list;

    public JsonArray(String json) {
        this.fromJson(json);
    }

    public JsonArray() {
        this.list = new ArrayList();
    }

    public JsonArray(List list) {
        this.list = list;
    }

    public String getString(int pos) {
        CharSequence cs = (CharSequence)this.list.get(pos);
        return cs == null ? null : cs.toString();
    }

    public Integer getInteger(int pos) {
        Number number = (Number)this.list.get(pos);
        if (number == null) {
            return null;
        } else {
            return number instanceof Integer ? (Integer)number : number.intValue();
        }
    }

    public Long getLong(int pos) {
        Number number = (Number)this.list.get(pos);
        if (number == null) {
            return null;
        } else {
            return number instanceof Long ? (Long)number : number.longValue();
        }
    }

    public Double getDouble(int pos) {
        Number number = (Number)this.list.get(pos);
        if (number == null) {
            return null;
        } else {
            return number instanceof Double ? (Double)number : number.doubleValue();
        }
    }

    public Float getFloat(int pos) {
        Number number = (Number)this.list.get(pos);
        if (number == null) {
            return null;
        } else {
            return number instanceof Float ? (Float)number : number.floatValue();
        }
    }

    public Boolean getBoolean(int pos) {
        return (Boolean)this.list.get(pos);
    }

    public JsonObject getJsonObject(int pos) {
        Object val = this.list.get(pos);
        if (val instanceof Map) {
            val = new JsonObject((Map)val);
        }

        return (JsonObject)val;
    }

    public JsonArray getJsonArray(int pos) {
        Object val = this.list.get(pos);
        if (val instanceof List) {
            val = new JsonArray((List)val);
        }

        return (JsonArray)val;
    }


    public Object getValue(int pos) {
        Object val = this.list.get(pos);
        if (val instanceof Map) {
            val = new JsonObject((Map)val);
        } else if (val instanceof List) {
            val = new JsonArray((List)val);
        }

        return val;
    }

    public boolean hasNull(int pos) {
        return this.list.get(pos) == null;
    }

    public JsonArray add(Enum value) {
        Objects.requireNonNull(value);
        this.list.add(value.name());
        return this;
    }

    public JsonArray add(CharSequence value) {
        Objects.requireNonNull(value);
        this.list.add(value.toString());
        return this;
    }

    public JsonArray add(String value) {
        Objects.requireNonNull(value);
        this.list.add(value);
        return this;
    }

    public JsonArray add(Integer value) {
        Objects.requireNonNull(value);
        this.list.add(value);
        return this;
    }

    public JsonArray add(Long value) {
        Objects.requireNonNull(value);
        this.list.add(value);
        return this;
    }

    public JsonArray add(Double value) {
        Objects.requireNonNull(value);
        this.list.add(value);
        return this;
    }

    public JsonArray add(Float value) {
        Objects.requireNonNull(value);
        this.list.add(value);
        return this;
    }

    public JsonArray add(Boolean value) {
        Objects.requireNonNull(value);
        this.list.add(value);
        return this;
    }

    public JsonArray addNull() {
        this.list.add((Object)null);
        return this;
    }

    public JsonArray add(JsonObject value) {
        Objects.requireNonNull(value);
        this.list.add(value);
        return this;
    }

    public JsonArray add(JsonArray value) {
        Objects.requireNonNull(value);
        this.list.add(value);
        return this;
    }


    public JsonArray add(Object value) {
        Objects.requireNonNull(value);
        value = Json.checkAndCopy(value, false);
        this.list.add(value);
        return this;
    }

    public JsonArray addAll(JsonArray array) {
        Objects.requireNonNull(array);
        this.list.addAll(array.list);
        return this;
    }

    public boolean contains(Object value) {
        return this.list.contains(value);
    }

    public boolean remove(Object value) {
        return this.list.remove(value);
    }

    public Object remove(int pos) {
        Object removed = this.list.remove(pos);
        if (removed instanceof Map) {
            return new JsonObject((Map)removed);
        } else {
            return removed instanceof ArrayList ? new JsonArray((List)removed) : removed;
        }
    }

    public int size() {
        return this.list.size();
    }

    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    public List getList() {
        return this.list;
    }

    public JsonArray clear() {
        this.list.clear();
        return this;
    }

    public Iterator<Object> iterator() {
        return new JsonArray.Iter(this.list.iterator());
    }

    public String encode() {
        return Json.encode(this.list);
    }

    public String encodePrettily() {
        return Json.encodePrettily(this.list);
    }

    public JsonArray copy() {
        List<Object> copiedList = new ArrayList(this.list.size());
        Iterator var2 = this.list.iterator();

        while(var2.hasNext()) {
            Object val = var2.next();
            val = Json.checkAndCopy(val, true);
            copiedList.add(val);
        }

        return new JsonArray(copiedList);
    }

    public Stream<Object> stream() {
        return Json.asStream(this.iterator());
    }

    public String toString() {
        return this.encode();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            return o != null && this.getClass() == o.getClass() ? arrayEquals(this.list, o) : false;
        }
    }

    static boolean arrayEquals(List<?> l1, Object o2) {
        List l2;
        if (o2 instanceof JsonArray) {
            l2 = ((JsonArray)o2).list;
        } else {
            if (!(o2 instanceof List)) {
                return false;
            }

            l2 = (List)o2;
        }

        if (l1.size() != l2.size()) {
            return false;
        } else {
            Iterator<?> iter = l2.iterator();
            Iterator var4 = l1.iterator();

            while(var4.hasNext()) {
                Object entry = var4.next();
                Object other = iter.next();
                if (entry == null) {
                    if (other != null) {
                        return false;
                    }
                } else if (!JsonObject.equals(entry, other)) {
                    return false;
                }
            }

            return true;
        }
    }

    public int hashCode() {
        return this.list.hashCode();
    }


    private void fromJson(String json) {
        this.list = (List)Json.decodeValue(json, List.class);
    }

    private class Iter implements Iterator<Object> {
        final Iterator<Object> listIter;

        Iter(Iterator<Object> listIter) {
            this.listIter = listIter;
        }

        public boolean hasNext() {
            return this.listIter.hasNext();
        }

        public Object next() {
            Object val = this.listIter.next();
            if (val instanceof Map) {
                val = new JsonObject((Map)val);
            } else if (val instanceof List) {
                val = new JsonArray((List)val);
            }

            return val;
        }

        public void remove() {
            this.listIter.remove();
        }
    }
}
