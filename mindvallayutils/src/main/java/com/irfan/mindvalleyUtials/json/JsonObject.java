package com.irfan.mindvalleyUtials.json;


import com.cellyco.mightyutils.MapUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

public class JsonObject implements Iterable<Map.Entry<String, Object>> {
    private Map<String, Object> map;

    public JsonObject(String json) {
        this.fromJson(json);
    }

    public JsonObject() {
        this.map = new LinkedHashMap();
    }

    public JsonObject(Map<String, Object> map) {
        this.map = map;
    }

    public static JsonObject mapFrom(Object obj) {
        return obj == null ? null : new JsonObject((Map)Json.mapper.convertValue(obj, Map.class));
    }

    public <T> T mapTo(Class<T> type) {
        return Json.mapper.convertValue(this.map, type);
    }

    public String getString(String key) {
        Objects.requireNonNull(key);
        CharSequence cs = (CharSequence)this.map.get(key);
        return cs == null ? null : cs.toString();
    }

    public Integer getInteger(String key) {
        Objects.requireNonNull(key);
        Number number = (Number)this.map.get(key);
        if (number == null) {
            return null;
        } else {
            return number instanceof Integer ? (Integer)number : number.intValue();
        }
    }

    public Long getLong(String key) {
        Objects.requireNonNull(key);
        Number number = (Number)this.map.get(key);
        if (number == null) {
            return null;
        } else {
            return number instanceof Long ? (Long)number : number.longValue();
        }
    }

    public Double getDouble(String key) {
        Objects.requireNonNull(key);
        Number number = (Number)this.map.get(key);
        if (number == null) {
            return null;
        } else {
            return number instanceof Double ? (Double)number : number.doubleValue();
        }
    }

    public Float getFloat(String key) {
        Objects.requireNonNull(key);
        Number number = (Number)this.map.get(key);
        if (number == null) {
            return null;
        } else {
            return number instanceof Float ? (Float)number : number.floatValue();
        }
    }

    public Boolean getBoolean(String key) {
        Objects.requireNonNull(key);
        return (Boolean)this.map.get(key);
    }

    public JsonObject getJsonObject(String key) {
        Objects.requireNonNull(key);
        Object val = this.map.get(key);
        if (val instanceof Map) {
            val = new JsonObject((Map)val);
        }

        return (JsonObject)val;
    }

    public JsonArray getJsonArray(String key) {
        Objects.requireNonNull(key);
        Object val = this.map.get(key);
        if (val instanceof List) {
            val = new JsonArray((List)val);
        }

        return (JsonArray)val;
    }


    public Object getValue(String key) {
        Objects.requireNonNull(key);
        Object val = this.map.get(key);
        if (val instanceof Map) {
            val = new JsonObject((Map)val);
        } else if (val instanceof List) {
            val = new JsonArray((List)val);
        }

        return val;
    }

    public String getString(String key, String def) {
        Objects.requireNonNull(key);
        CharSequence cs = (CharSequence)this.map.get(key);
        return cs == null && !this.map.containsKey(key) ? def : (cs == null ? null : cs.toString());
    }

    public Integer getInteger(String key, Integer def) {
        Objects.requireNonNull(key);
        Number val = (Number)this.map.get(key);
        if (val == null) {
            return this.map.containsKey(key) ? null : def;
        } else {
            return val instanceof Integer ? (Integer)val : val.intValue();
        }
    }

    public Long getLong(String key, Long def) {
        Objects.requireNonNull(key);
        Number val = (Number)this.map.get(key);
        if (val == null) {
            return this.map.containsKey(key) ? null : def;
        } else {
            return val instanceof Long ? (Long)val : val.longValue();
        }
    }

    public Double getDouble(String key, Double def) {
        Objects.requireNonNull(key);
        Number val = (Number)this.map.get(key);
        if (val == null) {
            return this.map.containsKey(key) ? null : def;
        } else {
            return val instanceof Double ? (Double)val : val.doubleValue();
        }
    }

    public Float getFloat(String key, Float def) {
        Objects.requireNonNull(key);
        Number val = (Number)this.map.get(key);
        if (val == null) {
            return this.map.containsKey(key) ? null : def;
        } else {
            return val instanceof Float ? (Float)val : val.floatValue();
        }
    }

    public Boolean getBoolean(String key, Boolean def) {
        Objects.requireNonNull(key);
        Object val = this.map.get(key);
        return val == null && !this.map.containsKey(key) ? def : (Boolean)val;
    }

    public JsonObject getJsonObject(String key, JsonObject def) {
        JsonObject val = this.getJsonObject(key);
        return val == null && !this.map.containsKey(key) ? def : val;
    }

    public JsonArray getJsonArray(String key, JsonArray def) {
        JsonArray val = this.getJsonArray(key);
        return val == null && !this.map.containsKey(key) ? def : val;
    }



    public Object getValue(String key, Object def) {
        Objects.requireNonNull(key);
        Object val = this.getValue(key);
        return val == null && !this.map.containsKey(key) ? def : val;
    }

    public boolean containsKey(String key) {
        Objects.requireNonNull(key);
        return this.map.containsKey(key);
    }

    public Set<String> fieldNames() {
        return this.map.keySet();
    }

    public JsonObject put(String key, Enum value) {
        Objects.requireNonNull(key);
        this.map.put(key, value == null ? null : value.name());
        return this;
    }

    public JsonObject put(String key, CharSequence value) {
        Objects.requireNonNull(key);
        this.map.put(key, value == null ? null : value.toString());
        return this;
    }

    public JsonObject put(String key, String value) {
        Objects.requireNonNull(key);
        this.map.put(key, value);
        return this;
    }

    public JsonObject put(String key, Integer value) {
        Objects.requireNonNull(key);
        this.map.put(key, value);
        return this;
    }

    public JsonObject put(String key, Long value) {
        Objects.requireNonNull(key);
        this.map.put(key, value);
        return this;
    }

    public JsonObject put(String key, Double value) {
        Objects.requireNonNull(key);
        this.map.put(key, value);
        return this;
    }

    public JsonObject put(String key, Float value) {
        Objects.requireNonNull(key);
        this.map.put(key, value);
        return this;
    }

    public JsonObject put(String key, Boolean value) {
        Objects.requireNonNull(key);
        this.map.put(key, value);
        return this;
    }

    public JsonObject putNull(String key) {
        Objects.requireNonNull(key);
        this.map.put(key, (Object)null);
        return this;
    }

    public JsonObject put(String key, JsonObject value) {
        Objects.requireNonNull(key);
        this.map.put(key, value);
        return this;
    }

    public JsonObject put(String key, JsonArray value) {
        Objects.requireNonNull(key);
        this.map.put(key, value);
        return this;
    }


    public JsonObject put(String key, Object value) {
        Objects.requireNonNull(key);
        value = Json.checkAndCopy(value, false);
        this.map.put(key, value);
        return this;
    }

    public Object remove(String key) {
        return this.map.remove(key);
    }

    public JsonObject mergeIn(JsonObject other) {
        return this.mergeIn(other, false);
    }

    public JsonObject mergeIn(JsonObject other, boolean deep) {
        return this.mergeIn(other, deep ? 2147483647 : 1);
    }

    public JsonObject mergeIn(JsonObject other, int depth) {
        if (depth < 1) {
            return this;
        } else if (depth == 1) {
            this.map.putAll(other.map);
            return this;
        } else {
            Iterator var3 = other.map.entrySet().iterator();

            while(var3.hasNext()) {
                Map.Entry<String, Object> e = (Map.Entry)var3.next();
                if (e.getValue() == null) {
                    this.map.put(e.getKey(), null);
                } else {

                    MapUtil.merge(this.map, e.getKey(), e.getValue(), (oldVal, newVal) -> {
                        if (oldVal instanceof Map) {
                            oldVal = new JsonObject((Map)oldVal);
                        }

                        if (newVal instanceof Map) {
                            newVal = new JsonObject((Map)newVal);
                        }

                        return oldVal instanceof JsonObject && newVal instanceof JsonObject ? ((JsonObject)oldVal).mergeIn((JsonObject)newVal, depth - 1) : newVal;
                    });
                }
            }

            return this;
        }
    }

    public String encode() {
        return Json.encode(this.map);
    }

    public String encodePrettily() {
        return Json.encodePrettily(this.map);
    }

    public JsonObject copy() {
        Object copiedMap;
        if (this.map instanceof LinkedHashMap) {
            copiedMap = new LinkedHashMap(this.map.size());
        } else {
            copiedMap = new HashMap(this.map.size());
        }

        Iterator var2 = this.map.entrySet().iterator();

        while(var2.hasNext()) {
            Map.Entry<String, Object> entry = (Map.Entry)var2.next();
            Object val = entry.getValue();
            val = Json.checkAndCopy(val, true);
            ((Map)copiedMap).put(entry.getKey(), val);
        }

        return new JsonObject((Map)copiedMap);
    }

    public Map<String, Object> getMap() {
        return this.map;
    }

    public Stream<Map.Entry<String, Object>> stream() {
        return Json.asStream(this.iterator());
    }

    public Iterator<Map.Entry<String, Object>> iterator() {
        return new JsonObject.Iter(this.map.entrySet().iterator());
    }

    public int size() {
        return this.map.size();
    }

    public JsonObject clear() {
        this.map.clear();
        return this;
    }

    public boolean isEmpty() {
        return this.map.isEmpty();
    }

    public String toString() {
        return this.encode();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            return o != null && this.getClass() == o.getClass() ? objectEquals(this.map, o) : false;
        }
    }

    static boolean objectEquals(Map<?, ?> m1, Object o2) {
        Map m2;
        if (o2 instanceof JsonObject) {
            m2 = ((JsonObject)o2).map;
        } else {
            if (!(o2 instanceof Map)) {
                return false;
            }

            m2 = (Map)o2;
        }

        if (m1.size() != m2.size()) {
            return false;
        } else {
            Iterator var3 = m1.entrySet().iterator();

            while(var3.hasNext()) {
                Map.Entry<?, ?> entry = (Map.Entry)var3.next();
                Object val = entry.getValue();
                if (val == null) {
                    if (m2.get(entry.getKey()) != null) {
                        return false;
                    }
                } else if (!equals(entry.getValue(), m2.get(entry.getKey()))) {
                    return false;
                }
            }

            return true;
        }
    }

    static boolean equals(Object o1, Object o2) {
        if (o1 == o2) {
            return true;
        } else if (o1 instanceof JsonObject) {
            return objectEquals(((JsonObject)o1).map, o2);
        } else if (o1 instanceof Map) {
            return objectEquals((Map)o1, o2);
        } else if (o1 instanceof JsonArray) {
            return JsonArray.arrayEquals(((JsonArray)o1).getList(), o2);
        } else if (o1 instanceof List) {
            return JsonArray.arrayEquals((List)o1, o2);
        } else if (o1 instanceof Number && o2 instanceof Number && o1.getClass() != o2.getClass()) {
            Number n1 = (Number)o1;
            Number n2 = (Number)o2;
            if (!(o1 instanceof Float) && !(o1 instanceof Double) && !(o2 instanceof Float) && !(o2 instanceof Double)) {
                return n1.longValue() == n2.longValue();
            } else {
                return n1.doubleValue() == n2.doubleValue();
            }
        } else {
            return o1.equals(o2);
        }
    }

    public int hashCode() {
        return this.map.hashCode();
    }


    private void fromJson(String json) {
        this.map = (Map)Json.decodeValue(json, Map.class);
    }

    private static final class Entry implements Map.Entry<String, Object> {
        final String key;
        final Object value;

        public Entry(String key, Object value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return this.key;
        }

        public Object getValue() {
            return this.value;
        }

        public Object setValue(Object value) {
            throw new UnsupportedOperationException();
        }
    }

    private class Iter implements Iterator<Map.Entry<String, Object>> {
        final Iterator<Map.Entry<String, Object>> mapIter;

        Iter(Iterator<Map.Entry<String, Object>> mapIter) {
            this.mapIter = mapIter;
        }

        public boolean hasNext() {
            return this.mapIter.hasNext();
        }

        public Map.Entry<String, Object> next() {
            Map.Entry<String, Object> entry = (Map.Entry)this.mapIter.next();
            if (entry.getValue() instanceof Map) {
                return new JsonObject.Entry((String)entry.getKey(), new JsonObject((Map)entry.getValue()));
            } else {
                return (Map.Entry)(entry.getValue() instanceof List ? new JsonObject.Entry((String)entry.getKey(), new JsonArray((List)entry.getValue())) : entry);
            }
        }

        public void remove() {
            this.mapIter.remove();
        }
    }
}
