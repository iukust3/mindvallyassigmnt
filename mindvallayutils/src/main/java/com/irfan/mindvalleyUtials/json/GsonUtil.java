package com.irfan.mindvalleyUtials.json;

import android.content.Context;
import android.widget.ImageView;

import com.cellyco.mightyutils.BiConsumer;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.irfan.mindvalleyUtials.interfaces.IJson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.LoadBuilder;


public class GsonUtil implements IJson {

    private static Gson gson = null;
    public static GsonUtil GSONUTIL;
    private static LoadBuilder loadBuilder;
    private static Context context;
    public static void iniate(Context context){
        if(GSONUTIL==null)
        GSONUTIL=new GsonUtil();
        GsonUtil.context=context;
    }

    public static final JsonObject EMPTY_JSON = new JsonObject();

    @Override
    public  com.google.gson.JsonObject toJson(com.irfan.mindvalleyUtials.json.JsonObject jsonObject) {
        JsonElement jsonElement = gson().toJsonTree(jsonObject.getMap());
        return jsonElement.getAsJsonObject();
    }

    @Override
    public void getJsonFromServer(String url, BiConsumer<JsonArray, Throwable> consumer) {
        Ion.with(context)
                .load(url)
                .asJsonArray()
                .setCallback((e, result) -> {
                    consumer.accept(result,e);
                });
    }

    public static final Gson gson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }
}
