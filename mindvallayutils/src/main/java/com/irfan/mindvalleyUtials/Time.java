package com.cellyco.mightyutils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class Time {
    public static void setTimeout(Runnable callback, long milliseconds){
        new android.os.Handler().postDelayed(callback,milliseconds);
    }

    public static long nowMillis(){
        return System.currentTimeMillis();
    }

    public static long nowSeconds(){
        return System.currentTimeMillis() / 1000;
    }

    public static String nowString() {
        return string(System.currentTimeMillis());
    }

    public static String string(long millisec){
        return string(millisec, "ru");
    }

    public static void setInterval(Runnable callback, long milliseconds){
        new Timer().scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                callback.run();
            }
        },0,milliseconds);
    }

    /**
     *
     * @param millisec  - milliseconds
     * @param locale - e.g. "ru"
     * @return
     */
    public static String string(long millisec, String locale){
        Date date = new Date(millisec);
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm dd.MMMM.yyyy", new Locale(locale));
        return dateFormat.format(date);
    }


}
