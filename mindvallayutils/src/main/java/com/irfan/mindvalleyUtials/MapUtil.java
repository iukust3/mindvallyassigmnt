package com.cellyco.mightyutils;

import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;

public class MapUtil {
    public static Object merge(Map<String, Object> map, String key, Object value, BiFunction<Object, Object, Object> remappingFunction) {
        Objects.requireNonNull(remappingFunction);
        Objects.requireNonNull(value);

        Object oldValue = map.get(key);
        Object newValue = (oldValue == null) ? value : remappingFunction.apply(oldValue, value);

        if (newValue == null) {
            map.remove(key);
        } else {
            map.put(key, newValue);
        }
        return newValue;
    }

}
