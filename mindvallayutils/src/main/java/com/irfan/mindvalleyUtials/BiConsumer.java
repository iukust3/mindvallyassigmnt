package com.cellyco.mightyutils;

@FunctionalInterface
public interface BiConsumer<T, U> {
    void accept(T var1, U var2);

    default BiConsumer<T, U> andThen(BiConsumer<? super T, ? super U> after) {
        throw new RuntimeException("Stub!");
    }
}
