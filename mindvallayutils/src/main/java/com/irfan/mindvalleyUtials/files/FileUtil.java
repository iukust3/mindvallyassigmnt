package com.irfan.mindvalleyUtials.files;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.cellyco.mightyutils.BiConsumer;
import com.irfan.mindvalleyUtials.json.JsonObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import androidx.arch.core.util.Function;

public class FileUtil {

    private static List<Function<File, Throwable>> readConditions = Arrays.asList(
            FileUtil::existCondition,
            FileUtil::canReadCondition
    );

    private static Throwable existCondition(File file) {
        return file.exists() ? null : new FileNotFoundException(file.getName());
    }

    private static Throwable canReadCondition(File file) {
        return file.canRead() ? null : new IOException("Can't read file: " + file.getName());
    }

    public static void writeText(String text, String filename, Context context, BiConsumer<String, Throwable> consumer){
        FileOutputStream fileOutputStream = null;

        try {
            File file = newFileInStorage(filename, context);
            file.setReadable(true, false);
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(text.getBytes(StandardCharsets.UTF_8));
            fileOutputStream.flush();
            consumer.accept(file.getAbsolutePath(), null);
        } catch (Throwable e) {
            e.printStackTrace();
            consumer.accept(null, e);
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static File newFileInStorage(String fileName, Context context) {
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir("files", Context.MODE_PRIVATE);
        return new File(directory, fileName);
    }

    public static void readJson(String fileName, Context context, BiConsumer<JsonObject, Throwable> consumer) {
        readText(fileName, context, (res, th) -> {
            if (th == null) {
                try {
                    consumer.accept(new JsonObject(res), null);
                } catch (Exception e) {
                    consumer.accept(null, e);
                }
            } else {
                consumer.accept(null, th);
            }
        });

    }

    public static void readText(String filename, Context context, BiConsumer<String, Throwable> consumer) {
        ContextWrapper contextWrapper = new ContextWrapper(context);
        File directory = contextWrapper.getDir("files", Context.MODE_PRIVATE);

        File file = new File(directory, filename);

        String result = null;
        Throwable th = getReadableException(file);

        if (th != null) {
            consumer.accept(null, th);
            return;
        }

        try {
             result = convertStreamToString(new FileInputStream(file));
        } catch (Throwable e) {
            th = e;
        } finally {
            consumer.accept(result, th);
        }
    }

    private static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static boolean exist(File file){
        return getReadableException(file) == null;
    }

    public static Throwable getReadableException(File file) {
        Throwable result = null;

        for (Function<File, Throwable> func :
                readConditions) {
            result = func.apply(file);
            if (result != null) {
                break;
            }
        }

        return result;
    }

    public static void saveBitmap(ImageView imageView, BiConsumer<String, Throwable> consumer){

        FileOutputStream fileOutputStream = null;
/*
        try {
            final Bitmap bitmap = Bitmap.createBitmap(imageView.getDrawable());
            imageView.setDrawingCacheEnabled(false);
//            File file = new File(Environment.getExternalStorageDirectory() + "/" + UUID.randomUUID());

            ContextWrapper cw = new ContextWrapper(imageView.getContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File file=new File(directory,UUID.randomUUID().toString());

            fileOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);

            consumer.accept(file.getAbsolutePath(), null);
        } catch (Throwable e) {
            e.printStackTrace();
            consumer.accept(null, e);
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
       // }
    }

    public static String fileNameFromURL(String url) {
        if (url == null) {
            return "";
        }
        try {
            URL resource = new URL(url);
            String host = resource.getHost();
            if (host.length() > 0 && url.endsWith(host)) {
                // handle ...example.com
                return "";
            }
        }
        catch(MalformedURLException e) {
            return "";
        }

        int startIndex = url.lastIndexOf('/') + 1;
        int length = url.length();

        // find end index for ?
        int lastQMPos = url.lastIndexOf('?');
        if (lastQMPos == -1) {
            lastQMPos = length;
        }

        // find end index for #
        int lastHashPos = url.lastIndexOf('#');
        if (lastHashPos == -1) {
            lastHashPos = length;
        }

        // calculate the end index
        int endIndex = Math.min(lastQMPos, lastHashPos);
        return url.substring(startIndex, endIndex);
    }
}
