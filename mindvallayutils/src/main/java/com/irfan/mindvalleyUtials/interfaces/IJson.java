package com.irfan.mindvalleyUtials.interfaces;

import com.cellyco.mightyutils.BiConsumer;
import com.google.gson.JsonArray;
import com.irfan.mindvalleyUtials.json.JsonObject;

public interface IJson {
     com.google.gson.JsonObject toJson(JsonObject jsonObject);
     void getJsonFromServer(String url, BiConsumer<JsonArray,Throwable> consumer);

}
