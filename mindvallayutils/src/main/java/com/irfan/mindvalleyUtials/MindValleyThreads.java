package com.cellyco.mightyutils;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
/**
 * This is to speedup upload and downloads
 * time by using multithreading
 * param Runnable to be used for a thread
 */
public class MindValleyThreads {

    private static MindValleyThreads inst;

    private MindValleyThreads() {}

    public static MindValleyThreads inst() {
        if (inst == null) {
            inst = new MindValleyThreads();
        }
        return inst;
    }

    private final Executor disk = new DiskExecutor();
    private final Executor background = Executors.newFixedThreadPool(cores() << 1);
    private final Executor main = new MainExecutor();

    public void onMain(Runnable runnable){
        main.execute(runnable);
    }

    public int cores(){
        return Runtime.getRuntime().availableProcessors();
    }

    public void onBackground(Runnable runnable){
        background.execute(runnable);
    }

    public void onDisk(Runnable runnable){
        disk.execute(runnable);
    }

    private static class MainExecutor implements Executor {

        private Handler handler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(@NonNull Runnable command) {
            handler.post(command);
        }
    }

    private static class DiskExecutor implements Executor {
        private final Executor mDiskIO;

        public DiskExecutor() {
            mDiskIO = Executors.newSingleThreadExecutor();
        }

        @Override
        public void execute(@NonNull Runnable command) {
            mDiskIO.execute(command);
        }
    }
}
