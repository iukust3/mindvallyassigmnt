package com.irfan.mindvalleyUtials.http;

public class HttpHeader {
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String AUTHORIZATION = "Authorization";
    public static final String APPLICATION_JSON = "application/json";
}
